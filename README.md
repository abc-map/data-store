<div>
  <img alt="Abc-Map" src="./assets/main-icon.png" />
</div>

# Abc-Map: Data Store

This repository contains all the data available in the datastore of the official
instance, [abc-map.fr](https://abc-map.fr).

You want to add something ? [Create an issue](https://gitlab.com/abc-map/data-store/-/issues/new)
